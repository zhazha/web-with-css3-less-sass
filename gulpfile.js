'use strict';

var gulp = require('gulp');

var pkg = require('./package.json');
var less = require('gulp-less');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var sourcemaps = require('gulp-sourcemaps');

//这个liveReload需要浏览器安装LiveReload的扩展
var paths = {
	sass: 'sass/*.scss',
	less: 'less/*.less',
	css_dest: 'build/css',
	less_dest: '/less',
	sass_dest: '/sass'
};

gulp.task('sass', function(){
	gulp.src(paths.sass)
	.pipe(sourcemaps.init())
	.pipe(sass(/*here add some config */).on('error',sass.logError))
	.pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest(paths.css_dest + paths.sass_dest))
	.pipe(livereload());

});

gulp.task('less', function () {
	gulp.src(paths.less)
	.pipe(sourcemaps.init())
	.pipe(less())
	.pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest(paths.css_dest + paths.less_dest))
	.pipe(livereload());
});



gulp.task('watch', function () {
	livereload.listen();
	gulp.watch(paths.sass,['sass']);
	gulp.watch(paths.less,['less']);
	gulp.watch(['build/css/*.css','css/*.css']).on('change', function(file) {
		livereload.listen();
		console.dir(file);
	});
});


gulp.task('default',['sass','less','watch']);
