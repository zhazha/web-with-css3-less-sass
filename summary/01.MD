
>遗憾的是fieldset:invalid不能在IE上生效，暂时不能解决。要是有人能帮助就好了。
虽说只是一个小粒子，包括的细节知识真的很多，就现在还记得一点整理一下吧：

form elements 有哪些？
最重要的表单元素是input\select\option\textarea\button\datalist\label\fieldset\lengend\form.

:invalid
根据input的类型在验证失效的情况下描述任何的input或者form元素。调整无效的域的样式帮助用户识别和改正错误。
这里就是想说一切的表单元素可以和invalid伪类连用。

在某些浏览器，当然不包括IE，invalid可以匹配有一个或者多个有无效值的input元素的form或fieldset。这也是本小李子部分不能支持IE的原因。

http://tympanus.net/codrops/css_reference/invalid/
css里content属性想要换行的时候可以使用 \a ,注意它的两边应该留有空格，否则按普通字符串处理。并且还要配合word-space使用：

   ``` css
   h1:before {
    display: block;
    text-align: center;
    white-space: pre; 
	/*兼容火狐*/
	white-space: pre-wrap;
    content: "chapter\A hoofdstuk\A chapitre"
}
content的内容也不会影响document树。

可以通过quotes属性指定引号，当然要配合open-quote和close-quote一起使用：
>html: 
<P><Q>Trøndere gråter når <Q>Vinsjan på kaia</Q> blir deklamert.</Q>
css:
html{ quotes: '"' '"' "'" "'" }
/* html { quotes: "«" "»" '"' '"' } */
q:before { content: open-quote }
q:after  { content: close-quote }
![此处输入图片的描述][1]这里的符号是不能够复制的，而且越往后成对引号标记就对应着越深的嵌套。
[生成content的参考链接][2]
  [counter-increment规范][3]
counter是自包含的，在后代元素或者是伪元素里使用counter-reset会重新创建一个counter实例。
The scope of a counter starts at the first element in the document that has a 'counter-reset' for that counter and includes the element's descendants and its following siblings with their descendants. However, it does not include any elements in the scope of a counter with the same name created by a 'counter-reset' on a later sibling of the element or by a later 'counter-reset' on the same element.
翻译：counter的作用域开始于文档中有那个counter的counter-reset的第一个元素，包括那个元素的后代以及后代的兄弟节点。但是不包括在这个作用域中由counter-reset重新创建的同名counter的元素。

If 'counter-increment' or 'content' on an element or pseudo-element refers to a counter that is not in the scope of any 'counter-reset', implementations should behave as though a 'counter-reset' had reset the counter to 0 on that element or pseudo-element.
翻译：如果引用counter的元素或者伪元素的counter-increment或者content不在counter-reset指定的scope中，这行为就相当于把元素或者伪元素的counter置成0.
An element that is not displayed ('display' set to 'none') cannot increment or reset a counter.
Elements with 'visibility' set to 'hidden', on the other hand, do increment counters.

Pseudo-elements that are not generated also cannot increment or reset a counter.
这句话是说这个伪元素不能变化，就是不能像hover 或者invalid valid可以变化所以它就不能用于counter-increment。
For example, the following does not increment 'heading':

h1::before {
    content: normal;
    counter-increment: heading;
}
这里有表单验证时可能需要的正则表达式：http://html5pattern.com/


float在文档流之外和fixed absolute脱离文档流是不一样的，说文档流是一条河的话，float就相当于河里的石头，
而另外的两个就相当于河上的桥。flaot：left 就像是在说这个石头放在河岸左边的水里，clear：left
这个左岸的石头只能放一个。